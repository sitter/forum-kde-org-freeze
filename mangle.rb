#!/usr/bin/env ruby
# frozen_string_literal: true

# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

require 'nokogiri'

Dir.chdir('/var/www/forum/')
header = Nokogiri::HTML::DocumentFragment.parse(<<~DIV)
  <div class="alert alert-error" style="text-align: center">
    This forum has been archived. All content is frozen. Please use <a href="https://discuss.kde.org/">KDE Discuss</a> instead.
  </div>
DIV

Dir.glob('phpbb3/forum.kde.org/*.html') do |f|
i = 0

  data = File.read(f)
  next if data.empty?

  doc = Nokogiri::HTML(data)
  doc.xpath('//script').remove
  doc.xpath('//div[contains(@id, "footerRow")]').remove
  doc.xpath('//div[contains(@id, "forum-actions-top")]').remove
  doc.xpath('//div[contains(@id, "forum-actions-bottom")]').remove
  doc.xpath('//div[contains(@id, "topic-actions-top")]').remove
  doc.xpath('//div[contains(@id, "topic-actions-bottom")]').remove
  doc.xpath('//form[contains(@id, "topic-search")]').remove
  doc.xpath('//form[contains(@id, "form-inline")]').remove
  doc.xpath('//form[contains(@class, "well form-inline form-small bottom-spacer")]').remove
  doc.xpath('//div[contains(@class, "row bookmarks")]').remove

  # search bar
  doc.xpath('//form[contains(@id, "search")]').remove
  # huge navbar at top
  doc.xpath('//div[contains(@class, "navbar navbar-static-top Neverland")]').remove
  # Login and register button
  doc.xpath('//ul[contains(@class, "nav nav-pills pull-right nomargin")]').remove
  # user avatar graphic, may be PPI
  doc.xpath('//a[contains(@class, "avatar")]').remove
  # tag box, depends on search
  doc.xpath('//div[contains(@class, "align-center grey-box")]').remove

  doc.xpath('//a').each do |element|
    href = element['href']
    next unless href

    element.replace(element.children.at(0)) if href&.include?('memberlist.php')
    element.replace(element.children.at(0)) if href&.include?('karma.php')
    element.replace(element.children.at(0)) if href&.include?('ucp.php')
    element.remove if href.include?('faq.php')
    element.remove if href.include?('policy.php')
    element.remove if href.include?('tour.php')
    element.remove if href.include?('rss.php')
  end

  doc.at('body')&.prepend_child(header.dup)

  File.write(f, doc.to_html)
  puts "processed #{i+=1}"
end
