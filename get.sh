#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

set -ex

cd /var/www/forum

rm -rf phpbb3
mkdir phpbb3
cd phpbb3

wget https://forum.kde.org  --save-cookies cookies --keep-session-cookies
wget https://forum.kde.org  --load-cookies cookies \
     --page-requisites --convert-links  --mirror --no-parent --reject-regex \
     '([&?]highlight=|[&?]order=|posting.php[?]|privmsg.php[?]|search.php[?]|[&?]mark=|[&?]view=|viewtopic\.php.*[&?]p=|file.php?avatar=)' \
     --rejected-log=rejected.log -o wget.log --server-response \
     --adjust-extension \
     --restrict-file-names=windows

# --wait=5
# --user-agent='Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)' \

# Regex notes
# - don't include highlights
# - don't include order changes
# - don't include posting page
# - don't include PM page
# - don't include search page
# - don't include marking
# - don't include view sets
# - don't include post highlights
# - don't download avatars, they may be PPI
